from dataclasses import field
from statistics import mode
from django import forms
from .models import contracts

class FormularioPost(forms.ModelForm):
    class Meta:
        model=contracts
        fields=('nombre','fecha','archivo')
        