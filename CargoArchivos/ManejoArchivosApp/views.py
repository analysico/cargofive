import imp
from django.shortcuts import redirect, render
from .forms import FormularioPost
from .models import rates,contracts

import pandas as pd
import openpyxl
from pandas import json_normalize

# Create your views here.

def home(request):
    return render(request,"ManejoArchivosApp/index.html")
  
def cargar(request):
     if request.method=='POST':
        form=FormularioPost(request.POST, request.FILES)
        if form.is_valid():
             
            formulario=form.save(commit=False)
            nombre_archivo=str(formulario.archivo)
            val_excel=nombre_archivo[-4:]
            
            if val_excel=="xlsx":
                formulario.save()
                df1 = pd.read_excel("media/archivos/"+nombre_archivo,engine='openpyxl')
                if ({'POL','POD','Curr.',"20'GP","40'GP","40'HC"}.issubset(df1.columns)):
                    
                    contrato=contracts.objects.get(nombre=formulario.nombre,fecha=formulario.fecha)
                    
                    #Se almacenan cada una de las rutas contenidas en el archivo a la tabla rates.
                    for i in df1.index:
                        ruta=rates()
                        ruta.origin=df1.loc[i,'POL']
                        ruta.destination=df1.loc[i,'POD']
                        ruta.currency=df1.loc[i,'Curr.']
                        ruta.twenty=df1.loc[i,"20'GP"]
                        ruta.forty=df1.loc[i,"40'GP"]
                        ruta.fortyhc=df1.loc[i,"40'HC"]
                        ruta.contract=contrato
                        ruta.save()
                    
                    tabla=rates.objects.select_related('contract').filter(contract_id=contrato.id)
                    
                    return render(request,"ManejoArchivosApp/cargar.html",{"form":form,"tabla":tabla})
                else:
                    mensaje="El archivo no contiene una o varias de las columnas requeridas para completar la operación (POL, POD, Curr., 20'GP, 40'GP o 40'HC )"
                    contrato=contracts.objects.get(nombre=formulario.nombre,fecha=formulario.fecha)
                    contrato.delete()
                    return render(request,"ManejoArchivosApp/cargar.html",{"form":form,"mensaje":mensaje})
            
            else:
                #return redirect('/cargar?no')
                mensaje="El archivo a cargar debe tener extensión xlsx"
                return render(request,"ManejoArchivosApp/cargar.html",{"form":form,"mensaje":mensaje})
     else:
        form=FormularioPost()
        return render(request,"ManejoArchivosApp/cargar.html",{"form":form})
    
def comparar(request):
    contratos=contracts.objects.all().order_by('-id')[:2]
    if len(contratos)==2:
        contrato1=str(contratos[0].archivo)
        contrato2=str(contratos[1].archivo)
        df1=pd.read_excel("media/"+contrato1,engine='openpyxl')
        df2=pd.read_excel("media/"+contrato2,engine='openpyxl')
        conteo=0
        for i in df1.index:
            llave1=str(df1.loc[i,'POL'])+str(df1.loc[i,'POD'])+str(df1.loc[i,'Routing'])
            for j in df2.index:
                llave2=str(df2.loc[j,'POL'])+str(df2.loc[j,'POD'])+str(df2.loc[j,'Routing'])
                if llave1==llave2:
                    if df1.loc[i,'Curr.']==df2.loc[j,'Curr.'] and df1.loc[i,"20'GP"]==df2.loc[j,"20'GP"] and df1.loc[i,"40'GP"]==df2.loc[j,"40'GP"] and df1.loc[i,"40'HC"]==df2.loc[j,"40'HC"]:
                        break
                    else:
                        conteo=conteo+1
                        break
            else:
                conteo=conteo+1
        form=FormularioPost()
        print(conteo)
        return redirect('/cargar?conteo='+str(conteo))
    else:
        return redirect('/cargar?contratos=0')

def documentacion(request):
    return render(request,"ManejoArchivosApp/documentacion.html")