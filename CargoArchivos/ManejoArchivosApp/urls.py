from django.urls import path
from ManejoArchivosApp import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns=[
    path('cargar',views.cargar,name="Cargar"),
    path('comparar',views.comparar,name="Comparar"),
    path('documentacion',views.documentacion,name="Documentacion"),
    path('',views.home,name="Home"),
]

urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)