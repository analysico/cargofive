from django.db import models

# Create your models here.
class contracts(models.Model):
    nombre=models.CharField(max_length=50)
    fecha=models.DateField()
    archivo=models.FileField(upload_to='archivos')
    
    class Meta:
        verbose_name='contract'
        verbose_name_plural='contracts'
        
class rates(models.Model):
    origin=models.CharField(max_length=50)
    destination=models.CharField(max_length=50)
    currency=models.CharField(max_length=50)
    twenty=models.CharField(max_length=50)
    forty=models.CharField(max_length=50)
    fortyhc=models.CharField(max_length=50)
    contract=models.ForeignKey(contracts, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name='rate'
        verbose_name_plural='rates'